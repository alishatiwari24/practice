import { uniq, cloneDeep } from 'lodash';

const reels = {
    "reel1": {
        "numberOfRows": 82,
        "symbolDistribution": [
                            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
        '56de42d7-1bec-413b-9355-992b88970644',
        '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
        '56de42d7-1bec-413b-9355-992b88970644',
        '78262d74-d750-4116-aa41-15a3b0154011',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        '56de42d7-1bec-413b-9355-992b88970644',
        '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
        '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
        '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
        '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        '56de42d7-1bec-413b-9355-992b88970644',
        '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
        '56de42d7-1bec-413b-9355-992b88970644',
        '78262d74-d750-4116-aa41-15a3b0154011',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
        '56de42d7-1bec-413b-9355-992b88970644',
        'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
        'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
        '78262d74-d750-4116-aa41-15a3b0154011',
        '56de42d7-1bec-413b-9355-992b88970644',
        'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
        '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '78262d74-d750-4116-aa41-15a3b0154011',
        '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
        'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
        '56de42d7-1bec-413b-9355-992b88970644',
        'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
        '78262d74-d750-4116-aa41-15a3b0154011',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
        'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '78262d74-d750-4116-aa41-15a3b0154011',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        '56de42d7-1bec-413b-9355-992b88970644',
        'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
        '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '78262d74-d750-4116-aa41-15a3b0154011',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
        'bc53bbeb-976d-407a-a07c-d31ad721e174',
        '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
        'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
        'b5a812b1-1c21-4b85-a172-530f35f991ef',
        '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
        '4eb13a7a-9765-4e8c-8c36-403e43cbe47c'
                        ]
                    },
"reel2": {
    "numberOfRows": 82,
        "symbolDistribution": [
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '56de42d7-1bec-413b-9355-992b88970644',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '56de42d7-1bec-413b-9355-992b88970644',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '56de42d7-1bec-413b-9355-992b88970644',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '56de42d7-1bec-413b-9355-992b88970644',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '56de42d7-1bec-413b-9355-992b88970644',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '56de42d7-1bec-413b-9355-992b88970644',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174'
        ]
},
"reel3": {
    "numberOfRows": 82,
        "symbolDistribution": [
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '56de42d7-1bec-413b-9355-992b88970644',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '56de42d7-1bec-413b-9355-992b88970644',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef'
        ]
},
"reel4": {
    "numberOfRows": 82,
        "symbolDistribution": [
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '56de42d7-1bec-413b-9355-992b88970644',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '56de42d7-1bec-413b-9355-992b88970644',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '56de42d7-1bec-413b-9355-992b88970644',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644'
        ]
},
"reel5": {
    "numberOfRows": 82,
        "symbolDistribution": [
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '56de42d7-1bec-413b-9355-992b88970644',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '78262d74-d750-4116-aa41-15a3b0154011',
            '56de42d7-1bec-413b-9355-992b88970644',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '56de42d7-1bec-413b-9355-992b88970644',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            '56de42d7-1bec-413b-9355-992b88970644',
            '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
            '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
            '78262d74-d750-4116-aa41-15a3b0154011',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
            'bc53bbeb-976d-407a-a07c-d31ad721e174',
            'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
            '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
            'b5a812b1-1c21-4b85-a172-530f35f991ef',
            'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
            '4eb13a7a-9765-4e8c-8c36-403e43cbe47c'
        ]
}
}

export const symbolObject = {
    "1ed95855-19f2-4bab-a37d-a8b5ddb88e81": {
        "symbolName": "Hi5",
        "symbolNumber": 6,
        "symbolType": "High Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    "42e36df6-0c74-40b1-ab05-c49d3514b4ef": {
        "symbolName": "Hi2",
        "symbolNumber": 3,
        "symbolType": "High Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2"
    },
    "4eb13a7a-9765-4e8c-8c36-403e43cbe47c": {
        "symbolName": "Low4",
        "symbolNumber": 11,
        "symbolType": "Low Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4"
    },
    "56de42d7-1bec-413b-9355-992b88970644": {
        "symbolName": "Low1",
        "symbolNumber": 8,
        "symbolType": "Low Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    "731b08d4-c19e-4081-ad3a-3dc83a8887c2": {
        "symbolName": "Hi6",
        "symbolNumber": 7,
        "symbolType": "High Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    "78262d74-d750-4116-aa41-15a3b0154011": {
        "symbolName": "Hi4",
        "symbolNumber": 5,
        "symbolType": "High Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4"
    },
    "8d08ecf9-841f-48c0-ad29-da3098ba1f91": {
        "symbolName": "Wild",
        "symbolNumber": 1,
        "symbolType": "Wild",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild",
        "doesWildPay": true,
        "isExpandingWild": true
    },
    "903f1f1a-3c0f-49c4-8782-ed133e4acee1": {
        "symbolName": "FreeSpins",
        "symbolNumber": 12,
        "symbolType": "Scatter",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins"
    },
    "b5a812b1-1c21-4b85-a172-530f35f991ef": {
        "symbolName": "Low2",
        "symbolNumber": 9,
        "symbolType": "Low Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9": {
        "symbolName": "Hi3",
        "symbolNumber": 4,
        "symbolType": "High Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    "bc53bbeb-976d-407a-a07c-d31ad721e174": {
        "symbolName": "Low3",
        "symbolNumber": 10,
        "symbolType": "Low Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    "f5ee797d-e0ac-416f-bf07-c3da32be14ae": {
        "symbolName": "Hi1",
        "symbolNumber": 2,
        "symbolType": "High Paying",
        "symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1"
    }
}

export const symbols = uniq(Object.keys(symbolObject));

const symCounts = {};
const countsWithName = {};

for (const reel in reels) {    
    symCounts[reel] = {};
    countsWithName[reel] = {};
    for (const symbol of uniq(reels[reel].symbolDistribution)) {
        symCounts[reel][symbol] = 0;
    };
    
    for (const symbolId of reels[reel].symbolDistribution) {        
        if (symCounts[reel][symbolId] >=0 ) {
            symCounts[reel][symbolId] +=1;
        }
    }
    Object.keys(symCounts[reel]).forEach(symbol => {
        countsWithName[reel][symbolObject[symbol].symbolName] = cloneDeep(symCounts[reel][symbol]);
    })
}
// console.log(countsWithName, 'sym cnts');

