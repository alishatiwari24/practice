import { Client } from 'elasticsearch';
import { symbols } from './symbol-counts';
import * as fs from 'fs';
import * as moment from 'moment';

const client = new Client({
    host: 'http://search-slot-simulation-staging-erqsblwstyclqrnqmtgkehvmp4.eu-west-1.es.amazonaws.com'
    // host: ':9200'
})

// client.index({
//     index: 'test',
//     type: '_doc',
//     body: { uname: 'Alisha', pass: 'Tiwari', type: 'user'}
// }, (err, res) => {
//         console.log(err, res, 'rrr111111');
// });

// client.index({
//     index: 'test',
//     type: '_doc',
//     body: { cname: 'STXYZ', addr: 'Shapath Hexa', type: 'company' }
// }, (err, res) => {
//         console.log(err, res, 'rrr');
// })

// client.search({
//     index: 'test',
//     body: {
//         query: {
//             match: {
//                 'type.keyword': 'company'
//             }
//         }
//     }
// }, (err, res) => {
//         console.log(err, res, 'search result');
// })

const symbolFreqMG = {}, symbolFreqFG = {};

const getSymDocCountQuery = (symbols: any) => {
    const aggs = {};

    for (const symbolId of symbols) {
        symbolFreqMG[symbolId] = {};
        symbolFreqFG[symbolId] = {};

        const field = `spinCounts.${symbolId}`;
        aggs[`${symbolId}:10`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 2
                    }
                }
            }
        },
            aggs[`${symbolId}:20`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 3
                    }
                }
            }
            },
            aggs[`${symbolId}:30`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 5
                    }
                }
            }
            },
            aggs[`${symbolId}:40`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 6
                    }
                }
            }
            },
            aggs[`${symbolId}:50`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 8
                    }
                }
            }
            },
            aggs[`${symbolId}:60`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 9
                    }
                }
            }
            },
            aggs[`${symbolId}:70`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 11
                    }
                }
            }
            },
            aggs[`${symbolId}:80`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 12
                    }
                }
            }
            },
            aggs[`${symbolId}:90`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 14
                    }
                }
            }
            },
            aggs[`${symbolId}:100`] = {
            filter: {
                range: {
                    [field]: {
                        gte: 15
                    }
                }
            }
            }
        
    }
    return aggs;
}

const performSearch = () => {
    const aggs = getSymDocCountQuery(symbols);

    client.search({
        index: 'simulation', type: 'simulation', body: {
            "query": {
                "match": {
                    "gameId.keyword": "075113a8-4052-4b6b-be22-09103d8d5782"
                }
            },
            aggs: {
                "mg": {
                    filter: {
                        bool: {
                            should: [{
                                term: {
                                    'freeSpinData.currentFreeSpin': 0,
                                },
                            }, {
                                bool: {
                                    must_not: {
                                        exists: {
                                            field: 'freeSpinData.currentFreeSpin',
                                        },
                                    },
                                },
                            }],
                        },
                    },
                    aggs
                },
                "fg": {
                    filter: {
                        range: {
                            'freeSpinData.currentFreeSpin': {
                                gt: 0
                            }
                        }
                    },
                    aggs
                }
            }
        }
    }, (err, data) => {
            if (err) console.log(err);
            console.log('mg spin counts = ', data.aggregations.mg.doc_count, 'fg spin counts = ', data.aggregations.fg.doc_count);
            let logAggrData = `DATE: ${moment(Date.now()).format('DD-MM-YYYY hh:mm:ss')}\n RTP: 94.86\n`;
            logAggrData += `***Arranged half amount of symbols together in line and remaining half elsewhere***\n`;
                logAggrData += `\nmg spin counts =  ${data.aggregations.mg.doc_count} \nfg spin counts =  ${data.aggregations.fg.doc_count}`
            logAggrData += `\n Main Game Doc counts of symbol \n ` + JSON.stringify(data.aggregations.mg);
            logAggrData += `\ Free Game Doc counts of symbol \n ` + JSON.stringify(data.aggregations.fg);
            Object.keys(data.aggregations.mg).forEach((symbolData) => {
                if (symbolData !== 'doc_count') {
                    const [symId, percent] = symbolData.split(':');
                    const freq = data.aggregations.mg.doc_count / data.aggregations.mg[symbolData].doc_count;
                    symbolFreqMG[symId][percent] = isNaN(freq) || !isFinite(freq) ? 0 : Math.round(freq * 100) / 100;
                }
            });
            
            Object.keys(data.aggregations.fg).forEach((symbolData) => {
                if (symbolData !== 'doc_count' && symbolData !== 'meta') {
                    const [symId, percent] = symbolData.split(':');
                    const freq = data.aggregations.fg.doc_count / data.aggregations.fg[symbolData].doc_count;
                    symbolFreqFG[symId][percent] = isNaN(freq) || !isFinite(freq) ? 0 : Math.round(freq * 100) / 100;
                }
            })
            console.log('MG symbolId Freq', symbolFreqMG, 'FG symbolId Freq', symbolFreqFG);
            logAggrData += `\nMG symbolId Freq\n  ${JSON.stringify(symbolFreqMG)} \nFG symbolId Freq\n ${JSON.stringify(symbolFreqFG)}`;
            fs.appendFileSync('prod-nearmiss1.txt', logAggrData);
    });
}

performSearch();