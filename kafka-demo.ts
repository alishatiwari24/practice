import * as kafka from 'kafka-node';
const client = new kafka.KafkaClient();
const producer = new kafka.Producer(client);
const consumer = new kafka.Consumer(client, [{ topic: 'user-token', partition: 0 }], {});
producer.on('ready', () => {
    producer.createTopics(['user-demo'], true, (error, data) => {
        if (error) {
            console.log(error, 'error');
        } else {
            console.log(data, 'data');
        }
    });
});
consumer.on('message', (message) => {
    console.log(message, 'message');
});
client.on('error', (err) => {
    console.log(err, 'err');
});
console.log(consumer.client, 'consumer');
