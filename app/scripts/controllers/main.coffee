'use strict'

###*
 # @ngdoc function
 # @name projectsApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the projectsApp
###
angular.module 'projectsApp'
  .controller 'MainCtrl', ->
    @awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
    return
