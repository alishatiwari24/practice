import * as gc from 'gc-profiler';

console.log(process.memoryUsage(), 'mem usage');
gc.on('gc', (info) => {
  console.log(info, 'gc info');
})
