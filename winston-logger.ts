import * as winston from 'winston';

/** creating a logger */
const logger = winston.createLogger({
    transports: [
        new winston.transports.Console(),
    ]
});

/** To log in format [ex- info: Hello info] */
// logger.format = winston.format.simple();
logger.format = winston.format.combine(winston.format.json(), winston.format.simple());

/**  log error on console */
logger.error({ message: 'hello error' });

/**  adding new transport */
logger.add(new winston.transports.File({ filename: 'info-log.txt' }));
logger.info({ message: 'Hello info' });

/** creating a child logger (To log warning in all parent transports + in warn-log file) */
const warnLogger = logger.child({});
warnLogger.add(new winston.transports.File({ filename: 'warn-log.txt' }));
warnLogger.warn('I m Warning You!!!');

/** another way to add transports in winLogger */
// logger.configure({
//     transports: new winston.transports.File({ filename: 'warn-log.txt' }),
//     level: 'warn'
// });

logger.silly('Silly :)');
logger.verbose('Its a verbose Message \n Yes it is!');
logger.debug('Lets start debuging');
