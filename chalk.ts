import chalk from 'chalk';

console.log(chalk.bgGreen.redBright.bold('Hello'));
console.log(chalk.keyword('orange')('Its too hot outside!!!'));

const symDist = [
    {
        "RowNumber": 0,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 1,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 2,
        "SymbolId": "f5ee797d-e0ac-416f-bf07-c3da32be14ae",
        "SymbolName": "Hi1",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1"
    },
    {
        "RowNumber": 3,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 4,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 5,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 6,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 7,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 8,
        "SymbolId": "78262d74-d750-4116-aa41-15a3b0154011",
        "SymbolName": "Hi4",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4"
    },
    {
        "RowNumber": 9,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 10,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 11,
        "SymbolId": "8d08ecf9-841f-48c0-ad29-da3098ba1f91",
        "SymbolName": "Wild",
        "SymbolType": "Wild",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild"
    },
    {
        "RowNumber": 12,
        "SymbolId": "8d08ecf9-841f-48c0-ad29-da3098ba1f91",
        "SymbolName": "Wild",
        "SymbolType": "Wild",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild"
    },
    {
        "RowNumber": 13,
        "SymbolId": "8d08ecf9-841f-48c0-ad29-da3098ba1f91",
        "SymbolName": "Wild",
        "SymbolType": "Wild",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild"
    },
    {
        "RowNumber": 14,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 15,
        "SymbolId": "78262d74-d750-4116-aa41-15a3b0154011",
        "SymbolName": "Hi4",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4"
    },
    {
        "RowNumber": 16,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 17,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 18,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 19,
        "SymbolId": "78262d74-d750-4116-aa41-15a3b0154011",
        "SymbolName": "Hi4",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4"
    },
    {
        "RowNumber": 20,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 21,
        "SymbolId": "f5ee797d-e0ac-416f-bf07-c3da32be14ae",
        "SymbolName": "Hi1",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1"
    },
    {
        "RowNumber": 22,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 23,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 24,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 25,
        "SymbolId": "f5ee797d-e0ac-416f-bf07-c3da32be14ae",
        "SymbolName": "Hi1",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1"
    },
    {
        "RowNumber": 26,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 27,
        "SymbolId": "78262d74-d750-4116-aa41-15a3b0154011",
        "SymbolName": "Hi4",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4"
    },
    {
        "RowNumber": 28,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 29,
        "SymbolId": "903f1f1a-3c0f-49c4-8782-ed133e4acee1",
        "SymbolName": "FreeSpins",
        "SymbolType": "Scatter",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins"
    },
    {
        "RowNumber": 30,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 31,
        "SymbolId": "42e36df6-0c74-40b1-ab05-c49d3514b4ef",
        "SymbolName": "Hi2",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2"
    },
    {
        "RowNumber": 32,
        "SymbolId": "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9",
        "SymbolName": "Hi3",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    {
        "RowNumber": 33,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 34,
        "SymbolId": "8d08ecf9-841f-48c0-ad29-da3098ba1f91",
        "SymbolName": "Wild",
        "SymbolType": "Wild",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild"
    },
    {
        "RowNumber": 35,
        "SymbolId": "8d08ecf9-841f-48c0-ad29-da3098ba1f91",
        "SymbolName": "Wild",
        "SymbolType": "Wild",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild"
    },
    {
        "RowNumber": 36,
        "SymbolId": "8d08ecf9-841f-48c0-ad29-da3098ba1f91",
        "SymbolName": "Wild",
        "SymbolType": "Wild",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild"
    },
    {
        "RowNumber": 37,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 38,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 39,
        "SymbolId": "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9",
        "SymbolName": "Hi3",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    {
        "RowNumber": 40,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 41,
        "SymbolId": "42e36df6-0c74-40b1-ab05-c49d3514b4ef",
        "SymbolName": "Hi2",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2"
    },
    {
        "RowNumber": 42,
        "SymbolId": "903f1f1a-3c0f-49c4-8782-ed133e4acee1",
        "SymbolName": "FreeSpins",
        "SymbolType": "Scatter",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins"
    },
    {
        "RowNumber": 43,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 44,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 45,
        "SymbolId": "42e36df6-0c74-40b1-ab05-c49d3514b4ef",
        "SymbolName": "Hi2",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2"
    },
    {
        "RowNumber": 46,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 47,
        "SymbolId": "f5ee797d-e0ac-416f-bf07-c3da32be14ae",
        "SymbolName": "Hi1",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1"
    },
    {
        "RowNumber": 48,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 49,
        "SymbolId": "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9",
        "SymbolName": "Hi3",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    {
        "RowNumber": 50,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 51,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 52,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 53,
        "SymbolId": "42e36df6-0c74-40b1-ab05-c49d3514b4ef",
        "SymbolName": "Hi2",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2"
    },
    {
        "RowNumber": 54,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 55,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 56,
        "SymbolId": "4eb13a7a-9765-4e8c-8c36-403e43cbe47c",
        "SymbolName": "Low4",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4"
    },
    {
        "RowNumber": 57,
        "SymbolId": "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9",
        "SymbolName": "Hi3",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    {
        "RowNumber": 58,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 59,
        "SymbolId": "903f1f1a-3c0f-49c4-8782-ed133e4acee1",
        "SymbolName": "FreeSpins",
        "SymbolType": "Scatter",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins"
    },
    {
        "RowNumber": 60,
        "SymbolId": "4eb13a7a-9765-4e8c-8c36-403e43cbe47c",
        "SymbolName": "Low4",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4"
    },
    {
        "RowNumber": 61,
        "SymbolId": "42e36df6-0c74-40b1-ab05-c49d3514b4ef",
        "SymbolName": "Hi2",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2"
    },
    {
        "RowNumber": 62,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 63,
        "SymbolId": "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9",
        "SymbolName": "Hi3",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    {
        "RowNumber": 64,
        "SymbolId": "4eb13a7a-9765-4e8c-8c36-403e43cbe47c",
        "SymbolName": "Low4",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4"
    },
    {
        "RowNumber": 65,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 66,
        "SymbolId": "56de42d7-1bec-413b-9355-992b88970644",
        "SymbolName": "Low1",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1"
    },
    {
        "RowNumber": 67,
        "SymbolId": "42e36df6-0c74-40b1-ab05-c49d3514b4ef",
        "SymbolName": "Hi2",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2"
    },
    {
        "RowNumber": 68,
        "SymbolId": "903f1f1a-3c0f-49c4-8782-ed133e4acee1",
        "SymbolName": "FreeSpins",
        "SymbolType": "Scatter",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins"
    },
    {
        "RowNumber": 69,
        "SymbolId": "78262d74-d750-4116-aa41-15a3b0154011",
        "SymbolName": "Hi4",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4"
    },
    {
        "RowNumber": 70,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 71,
        "SymbolId": "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9",
        "SymbolName": "Hi3",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    {
        "RowNumber": 72,
        "SymbolId": "4eb13a7a-9765-4e8c-8c36-403e43cbe47c",
        "SymbolName": "Low4",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4"
    },
    {
        "RowNumber": 73,
        "SymbolId": "78262d74-d750-4116-aa41-15a3b0154011",
        "SymbolName": "Hi4",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4"
    },
    {
        "RowNumber": 74,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 75,
        "SymbolId": "731b08d4-c19e-4081-ad3a-3dc83a8887c2",
        "SymbolName": "Hi6",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6"
    },
    {
        "RowNumber": 76,
        "SymbolId": "bc53bbeb-976d-407a-a07c-d31ad721e174",
        "SymbolName": "Low3",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3"
    },
    {
        "RowNumber": 77,
        "SymbolId": "bbcfd4cd-90da-4f24-a8e1-89c84c85cde9",
        "SymbolName": "Hi3",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3"
    },
    {
        "RowNumber": 78,
        "SymbolId": "1ed95855-19f2-4bab-a37d-a8b5ddb88e81",
        "SymbolName": "Hi5",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5"
    },
    {
        "RowNumber": 79,
        "SymbolId": "b5a812b1-1c21-4b85-a172-530f35f991ef",
        "SymbolName": "Low2",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2"
    },
    {
        "RowNumber": 80,
        "SymbolId": "f5ee797d-e0ac-416f-bf07-c3da32be14ae",
        "SymbolName": "Hi1",
        "SymbolType": "High Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1"
    },
    {
        "RowNumber": 81,
        "SymbolId": "4eb13a7a-9765-4e8c-8c36-403e43cbe47c",
        "SymbolName": "Low4",
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4"
    }
];

const newSymDist = [];

for (const sym of symDist) {
    newSymDist.push(sym.SymbolId);
}

// console.log(newSymDist, 'length');
const mysym = Symbol('flag');
console.log(mysym, typeof mysym, 'my symbol');

const cache = Object.create(null);

