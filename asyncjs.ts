import * as async from 'async';
import * as fs from 'fs';
import * as moment from 'moment';

let files = ['chalk.ts', 'debug-demo.ts']
const existingFiles = [];
// async.map(files, fs.stat, (err, data) => {
//     if (err) console.log(err, 'errr');
//     console.log(data, 'data');
// })

async.series([(cb) => {
    console.log(existingFiles, 'existing files before');
    files.forEach(file => {
        if (fs.existsSync(file) === true) {
            existingFiles.push(file);
        }
    });
    files = existingFiles;
    cb(null, 'DONE');
}, (cb) => {
    console.log(existingFiles, 'existing files');
    // files.forEach(file => {
    //     fs.readFile(file, (err, data) => {
    //         if (err) console.log(err, 'err in read');
    //         console.log(data.toString());
    //     });
    // });

}], (err, res) => {
    console.log(res, 'final results');
})

async.each(files, (item, cb) => {
    fs.readFile(item, (err, data) => {
        if (err) console.log(err, 'err in read');
        console.log(data.toString());
        cb(null);
    });
});

const add1 = (n, callback) => {
    setTimeout(() => {
        callback(null, n + 1);
    }, 10);
}

const mul3 = (n, callback) => {
    setTimeout(() => {
        callback(null, n * 3);
    }, 10);
}
/** composing two methods 2gether
 *  here, first `add1()` is applied to input
 *  and then `mul3()` is applied to result of `add1()`
 */
const add1mul3 = async.compose(mul3, add1);

add1mul3(4, (err, ans) => {
    console.log(err, ans, 'answer of add1mul3');
})

/** result of first callback is passed as argument to second callback and so on */
async.waterfall([(cb) => {
    cb(null, 4)
}, (n, cb) => {
    cb(null, n * 3, 1)
}, (n, addBy, cb) => {
    cb(null, n + addBy)
}], (err, res) => {
    if (err) console.log(err, 'errr occured in waterfall');
    console.log(res, 'answer of waterfall');
})

/** implementation of async queue to perform some arthmetic computations */
let n = 0;
const queue = async.queue((task, cb) => {
    console.log('queue task added: ', task);
    cb()
}, 1)

queue.push({ task: 1 }, (err) => {
    if (err) console.log('err in pushing task1');
    n *= 3;
})

queue.push({ task: 2 }, (err) => {
    if (err) console.log('err in pushing task2');
    n += 1;
    console.log('now n is: ', n);
})

if (moment().format('ddd') === 'Wed') {
    queue.unshift({ task: 3 }, (err) => {
        if (err) console.log(err, 'err in pushing task at start');
        n = 4;
    })
}

console.log(`queue length is ${queue.length()}`);
console.log(queue.running(), 'running tasks');



