import * as _ from 'lodash';

const symNameArray = [
'Lemon',
'Lemon',
'Lemon',
'Lemon',
'Seven',
'Seven',
'Seven',
'Seven',
'Orange',
'Orange',
'Orange',
'Orange',
'Blueberry',
'Blueberry',
'Blueberry',
'Blueberry',
'Lemon',
'Lemon',
'Lemon',
'Lemon',
'Lemon',
'Orange',
'Orange',
'Orange',
'Orange',
'Watermelon',
'Watermelon',
'Watermelon',
'Watermelon',
'Star',
'Star',
'Star',
'Star',
'Lemon',
'Lemon',
'Lemon',
'Lemon',
'Lemon',
'Orange',
'Orange',
'Orange',
'Orange',
'Bell',
'Bell',
'Bell',
'Bell',
'Cherry',
'Cherry',
'Cherry',
'Cherry',
];
const symbolObject = {
    "845bd4bf-5af9-4755-8731-e2e12071b460": {
        "SymbolName": "Lemon",
        "SymbolNumber": 1,
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://i.ibb.co/VWLGtqF/1.png"
    },
    "1403a005-2deb-47e0-8d57-a19c25c0a661": {
        "SymbolName": "Cherry",
        "SymbolNumber": 2,
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://i.ibb.co/dtF69Ch/2.png"
    },
    "f0c06993-cd32-4847-9978-b4d7397ee713": {
        "SymbolName": "Wild",
        "SymbolNumber": 9,
        "SymbolType": "Wild",
        "SymbolUrl": "https://i.ibb.co/bXD76PH/wild.png",
        "doesWildPay": false,
        "isExpandingWild": true
    },
    "6a3ad0d5-3861-4971-bb56-2a155e2013fd": {
        "SymbolName": "Orange",
        "SymbolNumber": 3,
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://i.ibb.co/znDg32c/3.png"
    },
    "13aadfba-2351-4a0b-a807-12057bb3dc9b": {
        "SymbolName": "Blueberry",
        "SymbolNumber": 4,
        "SymbolType": "Low Paying",
        "SymbolUrl": "https://i.ibb.co/Ksnz1vv/4.png"
    },
    "44284c03-c962-4ddb-9a9e-666cc69ba86f": {
        "SymbolName": "Watermelon",
        "SymbolNumber": 5,
        "SymbolType": "Medium Paying",
        "SymbolUrl": "https://i.ibb.co/6BBmJSV/5.png"
    },
    "3011bfab-8cc7-4e28-bcd3-406cae6593e4": {
        "SymbolName": "Star",
        "SymbolNumber": 6,
        "SymbolType": "Medium Paying",
        "SymbolUrl": "https://i.ibb.co/HB9sqKM/6.png"
    },
    "319acbcd-1f64-4653-b3cb-f2f63843d5b8": {
        "SymbolName": "Bell",
        "SymbolNumber": 7,
        "SymbolType": "High Paying",
        "SymbolUrl": "https://i.ibb.co/NC4DNwM/7.png"
    },
    "20ed76ad-b108-4850-bf04-35b9971e1483": {
        "SymbolName": "Seven",
        "SymbolNumber": 8,
        "SymbolType": "High Paying",
        "SymbolUrl": "https://i.ibb.co/KrG7V1X/8.png"
    }
};

const dbobject = [];

for (let index = 0; index < symNameArray.length; index++) {
    const symId = _.findKey(symbolObject, { SymbolName: symNameArray[index] });

    dbobject.push({
        RowNumber: index,
        SymbolId: symId,
        SymbolName: symbolObject[symId].SymbolName,
        SymbolType: symbolObject[symId].SymbolType,
        SymbolUrl: symbolObject[symId].SymbolUrl,
    });
}

console.log(JSON.stringify(dbobject));
