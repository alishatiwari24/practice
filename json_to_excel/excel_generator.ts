import * as excel from 'exceljs';
import * as express from 'express';
import { levelJSON } from './levelBetMultipliers';

const app = express();
app.listen(3030, 'localhost', (err, res) => {
    if (res) console.log(res, 'listening');
});
class GenerateExcel {
    createWorkBook = () => {
        
        const workbook = new excel.Workbook();
        const sheet = workbook.addWorksheet('bet-multipliers');
        sheet.columns = [
            { header: 'Level', key: 'level' },
            {
                header: 'Multipliers', key: 'multipliers'
            }
        ]
        const rows = [];
        for (const level in levelJSON) {
            rows.push({level,multipliers: levelJSON[level]})
        }
        sheet.addRows(rows);
        return { workbook, fileName: 'bet-multipliers'};
    }
    public async export(req: express.Request, res: express.Response) {
        const { workbook, fileName } = new GenerateExcel().createWorkBook();
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", `attachment; filename=${fileName}.xlsx`);
        workbook.xlsx.write(res).then(() => {
            res.end();
        });
    }
}

app.get('/excel', new GenerateExcel().export);

